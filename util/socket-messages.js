var net = require("net");
var JsonSocket = require("json-socket");
var SocketRouter = require("socket-router");

class SocketClient {
	constructor(host, port) {
		this._jsonSocket = new JsonSocket(new net.Socket());
		JsonSocket.prototype.send = JsonSocket.prototype.sendMessage;
		var socketClient;
		this._jsonSocket.connect(port, host);
		this._client = null;
	}

	message(route, data = {}) {
		return new Promise((resolve, reject) => {
			if (this._client == null) {
				this._client = new SocketRouter.Client(this._jsonSocket);
			}
			this._jsonSocket.on("error", (err) => {
				reject(err);
			})
			this._client.send(route, data, (err, response) => {
				if (err) reject(err);
				resolve(response);
			});
		});
	}

	close() {
		this._jsonSocket.end();
	}
}

function client(host, port) {
	return new SocketClient(host, port);
}

module.exports.client = client;