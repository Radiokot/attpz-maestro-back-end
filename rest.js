var errorCodes = require("./util/error-codes.js");
var restUtil = require("./rest/rest-util.js");
var os = require("os");

// HTTP things
var express = require("express")
var bodyParser = require("body-parser");
var router = express.Router();

var app = express();
var port = process.env.PORT || 84;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use("/attpz", router);

var errorHandler = function (err, req, res, next) {
	restUtil.apiResponse.error(res, {code: errorCodes.REST.PROCESS_REQUEST_FAILED, message: "Cant process request"});
};
app.use("/attpz", errorHandler);

var hostname = os.hostname();
var cors = function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "http://maestro.radiokot.com.ua");
	res.header("Access-Control-Allow-Headers", "X-Auth-Email, X-Auth-Key, Content-Type, User-Agent, X-Requested-With, Cache-Control");
	res.header("Access-Control-Allow-Credentials", "true");
	res.header("Access-Control-Allow-Methods", "POST, GET, PATCH, OPTIONS");
	res.header("X-Backend-Server", hostname);
	next();
};
router.use(cors);

app.listen(port);

// Users endpoints
var usersRoutes = require("./rest/users-rest-routes.js");
usersRoutes.configure(router);

// Instruments endpoints
var instrumentsRoutes = require("./rest/instruments-rest-routes.js");
instrumentsRoutes.configure(router);

// Instrument types endpoints
var instrumentTypesRoutes = require("./rest/instrument-types-rest-routes");
instrumentTypesRoutes.configure(router);

// Profiles endpoints
var profilesRoutes = require("./rest/profiles-rest-routes");
profilesRoutes.configure(router);

// Longpoll endpoints
var longpollRoutes = require("./rest/longpoll-routes");
longpollRoutes.configure(router);

console.log("HTTP REST instance is running on port " + port);