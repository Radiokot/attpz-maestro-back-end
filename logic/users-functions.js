var socketMessages = require("../util/socket-messages.js");
var errorCodes = require("../util/error-codes.js");
var logicUtil = require("../logic/logic-util.js");
var isEmpty = require("is-empty");
var sha256 = require("sha256");
var validateJson = require("jsonschema").validate;
var logicResponse = logicUtil.logicResponse;
var getDataClient = logicUtil.getDataClient;

class Users {
	// Create user from registration data.
	static createUser(registerData) {
		var user = {};
		user.salt = Math.random().toString(36).substring(2);
		user.password = sha256(user.salt + registerData.password);
		user.name = registerData.name;
		user.email = registerData.email;

		return user;
	}

	static validateRegisterData(registerData) {
		var userSchema = {
			"$schema": "http://json-schema.org/draft-04/schema#",
			"title": "User",
			"type": "object",
			"properties": {
				"name": {
					"type": "string",
					"minLength": 5,
					"maxLength": 100
				},
				"email": {
					"type": "string",
					"maxLength": 254,
					"pattern": "^[\\w,\\.]+?@\\w+\\.[\\w,\\.]+$"
				},
				"password": {
					"type": "string",
					"minLength": 8,
					"maxLength": 64
				}
			},
			"required": [
				"name",
				"email",
				"password"
			]
		};
		return validateJson(registerData, userSchema);
	}

	static getByEmail(email) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("get_user_by_email", {email: email})
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static add(user) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("add_user", {user: user})
				.then((res) => {
					// Return API credentials
					dataClient.close();
					resolve(logicResponse.ok(res));
				})
				.catch((error) => {
					client.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}


	static register(registerData) {
		return new Promise((resolve, reject) => {
			// Check if registration data invalid.
			var dataValidation = Users.validateRegisterData(registerData);
			if (dataValidation.errors.length > 0) {
				reject(logicResponse.error(errorCodes.LOGIC.INVALID_REQUEST_DATA, dataValidation.errors));
				return;
			}

			registerData.email = registerData.email.toLowerCase();

			// Check if email already registred.
			Users.getByEmail(registerData.email)
				.then((response) => {
					if (!isEmpty(response.response)) {
						reject(logicResponse.error(errorCodes.LOGIC.INVALID_REQUEST_DATA, "Email already registred"));
					} else {
						// We can create new user.
						var user = Users.createUser(registerData);
						Users.add(user)
							.then((response) => {
								resolve(response);
							})
							.catch((error) => {
								reject(error);
							});
					}
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	static login(email, password) {
		return new Promise((resolve, reject) => {
			if (isEmpty(email) || isEmpty(password)) {
				reject(logicResponse.error(errorCodes.LOGIC.INVALID_REQUEST_DATA, "Invalid user data"));
				return;
			}

			// Check if user exists.
			Users.getByEmail(email)
				.then((response) => {
					var user = response.response;

					if (isEmpty(user)) {
						reject(logicResponse.error(errorCodes.LOGIC.INVALID_REQUEST_DATA, "No such user"));
					} else {
						// If the user exists then check password.
						var hash = sha256(user.salt + password);
						if (hash == user.password) {
							resolve(logicResponse.ok({
								id: user.id,
								name: user.name,
								email: user.email,
								key: sha256(user.email + user.salt + user.password)
							}));
						} else {
							reject(logicResponse.error(errorCodes.LOGIC.INVALID_REQUEST_DATA, "Wrong password"));
						}
					}
				})
				.catch((error) => {
					reject(error);
				});
		});
	}
}

module.exports = Users;