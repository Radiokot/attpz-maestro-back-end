var errorCodes = require("../util/error-codes.js");
var logicUtil = require("../logic/logic-util.js");
var isEmpty = require("is-empty");
var logicResponse = logicUtil.logicResponse;
var getAuthorizedUser = logicUtil.getAuthorizedUser;

var Instruments = require("./instruments-functions.js");

function configure(server) {
	server.route("get_instruments_for_user", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				Instruments.getForUser(user)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("get_instrument_by_id", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				Instruments.getById(user, data.id)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("update_instrument", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				Instruments.update(user, data.id, data.update)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});
}

module.exports.configure = configure;