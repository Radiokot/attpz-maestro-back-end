var socketMessages = require("../util/socket-messages.js");
var errorCodes = require("../util/error-codes.js");
var logicUtil = require("../logic/logic-util.js");
var isEmpty = require("is-empty");
var jsonPatch = require("fast-json-patch");
var validateJson = require("jsonschema").validate;
var logicResponse = logicUtil.logicResponse;
var getDataClient = logicUtil.getDataClient;

var InstrumentTypes = require("./instrument-types-functions.js");

class Instruments {
	static getForUser(user) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("get_instruments_by_user_id", {userId: user.id})
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static getById(user, id) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("get_instrument_by_id", {id: id})
				.then((dataResponse) => {
					dataClient.close();

					var instrument = dataResponse;
					if (isEmpty(instrument) || instrument.userId != user.id) {
						reject(logicResponse.error(errorCodes.LOGIC.FORBIDDEN, "Forbidden"));
						return;
					}

					resolve(logicResponse.ok(instrument));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static update(user, id, update) {
		return new Promise((resolve, reject) => {
			Instruments.getById(user, id)
				.then((response) => {
					var instrument = response.response;

					if (isEmpty(instrument) || instrument.userId != user.id) {
						reject(logicResponse.error(errorCodes.LOGIC.FORBIDDEN, "Forbidden"));
						return;
					}

					// Get type schema for validation.
					InstrumentTypes.getById(instrument.type.id)
						.then((typeResponse) => {
							var type = typeResponse.response;

							// Delete fields, that cant be updated.
							update.id = update.userId = update.type = undefined;

							var prePatches = jsonPatch.compare(instrument, update);
							var patches = [];

							// Apply only "replace" patches.
							for (var patch of prePatches) {
								if (patch.op === 'replace') {
									patches[patches.length] = patch;
								}
							}

							jsonPatch.apply(instrument, patches);

							// Validate settings after patch.
							var validationResult = validateJson(instrument.settings, type.schema);
							if (validationResult.errors.length > 0) {
								reject(logicResponse.error(errorCodes.LOGIC.INVALID_REQUEST_DATA,
									"Settings dont match type's schema"));
								return;
							}

							var dataClient = getDataClient();
							dataClient.message("update_instrument", {instrument: instrument})
								.then((dataResponse) => {
									dataClient.close();
									resolve(logicResponse.ok({changes: patches.length}));
								})
								.catch((error) => {
									dataClient.close();
									reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
								});
						})
						.catch((error) => {
							reject(error)
						});
				})
				.catch((error) => {
					reject(error);
				});
		});
	}
}

module.exports = Instruments;