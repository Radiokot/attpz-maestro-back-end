var socketMessages = require("../util/socket-messages.js");
var errorCodes = require("../util/error-codes.js");
var isEmpty = require("is-empty");
var sha256 = require("sha256");

class LogicResponse {
	static ok(data) {
		return {response: data};
	}

	static error(errorCode, errorMsg = null) {
		var response = {error: {code: errorCode}};
		if (errorMsg != null) {
			response.error.message = errorMsg;
		}
		return response;
	}
}

var logicResponse = new LogicResponse();

function getDataClient() {
	return socketMessages.client("127.0.0.1", 8082);
}

// Get user object from auth credentials.
function getAuthorizedUser(authData) {
	return new Promise((resolve, reject) => {
		if (isEmpty(authData.email) || isEmpty(authData.key)) {
			resolve(null);
			return;
		}

		var dataClient = getDataClient();
		dataClient.message("get_user_by_email", {email: authData.email})
			.then((response) => {
				dataClient.close();
				if (isEmpty(response)) {
					resolve(null);
				} else {
					// If the user exists then check key.
					var user = response;
					var hash = sha256(user.email + user.salt + user.password);

					if (hash == authData.key) {
						resolve(user);
					} else {
						resolve(null);
					}
				}
			})
			.catch((error) => {
				dataClient.close();
				reject(error);
			});
	});
}

module.exports.logicResponse = LogicResponse;
module.exports.getDataClient = getDataClient;
module.exports.getAuthorizedUser = getAuthorizedUser;