var socketMessages = require("../util/socket-messages.js");
var errorCodes = require("../util/error-codes.js");
var logicUtil = require("../logic/logic-util.js");
var isEmpty = require("is-empty");
var jsonPatch = require("fast-json-patch");
var validateJson = require("jsonschema").validate;
var logicResponse = logicUtil.logicResponse;
var getDataClient = logicUtil.getDataClient;

var Instruments = require("./instruments-functions");
var InstrumentTypes = require("./instrument-types-functions.js");

class ProfileItems {
	static getForProfile(profileId) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("get_profile_items_by_profile_id", {profileId: profileId})
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static getItemInstruments(itemId) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("get_profile_item_intsruments", {itemId: itemId})
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static add(user, profileId, itemData) {
		return new Promise((resolve, reject) => {
			var item = {
				profileId: profileId,
				name: (!isEmpty(itemData.name))
					? itemData.name
					: ""
			}

			var dataClient = getDataClient();
			dataClient.message("add_profile_item", {item: item})
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static delete(user, itemId) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("delete_profile_item", {itemId: itemId})
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static addInstrument(user, itemId, instrumentData) {
		return new Promise((resolve, reject) => {
			Instruments.getById(user, instrumentData.id)
				.then((response) => {
					var instrument = response.response;

					if (isEmpty(instrument) || instrument.userId != user.id) {
						reject(logicResponse.error(errorCodes.LOGIC.FORBIDDEN, "Forbidden"));
						return;
					}

					// Get type schema for validation.
					InstrumentTypes.getById(instrument.type.id)
						.then((typeResponse) => {
							var type = typeResponse.response;

							// Validate settings.
							var validationResult = validateJson(instrumentData.settings, type.schema);
							if (validationResult.errors.length > 0) {
								reject(logicResponse.error(errorCodes.LOGIC.INVALID_REQUEST_DATA,
									"Settings dont match type's schema"));
								return;
							}

							var dataClient = getDataClient();
							dataClient.message("add_profile_item_instrument", {
								itemId: itemId,
								instrument: instrumentData
							})
								.then((dataResponse) => {
									dataClient.close();
									resolve(logicResponse.ok(dataResponse));
								})
								.catch((error) => {
									dataClient.close();
									reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
								});
						})
						.catch((error) => {
							reject(error)
						});
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	static apply(user, itemId) {
		return new Promise((resolve, reject) => {
			ProfileItems.getItemInstruments(itemId)
				.then((response) => {
					let instruments = response.response;
					let updates = [];

					for (let instrument of instruments) {
						updates[updates.length] = Instruments.update(user, instrument.id, instrument);
					}

					Promise.all(updates)
						.then((result) => {
							resolve(logicResponse.ok({"success": true}));
						})
						.catch((error) => {
							reject(error);
						});
				})
				.catch((error) => {
					reject(error);
				});
		});
	}
}

module.exports = ProfileItems;