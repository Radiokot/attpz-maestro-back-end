var socketMessages = require("../util/socket-messages.js");
var errorCodes = require("../util/error-codes.js");
var logicUtil = require("../logic/logic-util.js");
var isEmpty = require("is-empty");
var logicResponse = logicUtil.logicResponse;
var getDataClient = logicUtil.getDataClient;
var getAuthorizedUser = logicUtil.getAuthorizedUser;

var InstrumentTypes = require("./instrument-types-functions.js");

function configure(server) {
	server.route("get_instrument_types", (data, reply) => {
		InstrumentTypes.getAll()
			.then((response) => {
				reply(response);
			})
			.catch((error) => {
				reply(error);
			});
	});

	server.route("get_instrument_type_by_id", (data, reply) => {
		InstrumentTypes.getById(data.id)
			.then((response) => {
				reply(response);
			})
			.catch((error) => {
				reply(error);
			});
	});
}

module.exports.configure = configure;