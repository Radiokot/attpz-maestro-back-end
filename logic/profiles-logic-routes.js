var errorCodes = require("../util/error-codes.js");
var logicUtil = require("../logic/logic-util.js");
var isEmpty = require("is-empty");
var logicResponse = logicUtil.logicResponse;
var getAuthorizedUser = logicUtil.getAuthorizedUser;

var Profiles = require("./profiles-functions.js");

function configure(server) {
	server.route("get_profiles_for_user", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				Profiles.getForUser(user)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("get_profile_by_id", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				Profiles.getById(user, data.profileId)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("add_profile", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				Profiles.add(user, data.profileData)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("update_profile", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				Profiles.update(user, data.profileId, data.update)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("delete_profile", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				Profiles.delete(user, data.profileId)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});
}

module.exports.configure = configure;