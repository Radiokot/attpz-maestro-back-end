var socketMessages = require("../util/socket-messages.js");
var errorCodes = require("../util/error-codes.js");
var logicUtil = require("../logic/logic-util.js");
var isEmpty = require("is-empty");
var jsonPatch = require("fast-json-patch");
var validateJson = require("jsonschema").validate;
var logicResponse = logicUtil.logicResponse;
var getDataClient = logicUtil.getDataClient;

var ProfileItems = require("./profile-items-functions");

class Profiles {
	static getForUser(user) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("get_profiles_by_user_id", {userId: user.id})
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static getById(user, id) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("get_profile_by_id", {id: id})
				.then((dataResponse) => {

					var profile = dataResponse;
					if (isEmpty(profile) || profile.userId != user.id) {
						reject(logicResponse.error(errorCodes.LOGIC.FORBIDDEN, "Forbidden"));
						return;
					}

					ProfileItems.getForProfile(profile.id)
						.then((response) => {
							var items = response.response;
							profile.items = items;
							resolve(logicResponse.ok(profile));
						})
						.catch((error) => {
							reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
						});

					dataClient.close();
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static add(user, profileData) {
		return new Promise((resolve, reject) => {
			var profile = {
				userId: user.id,
				name: (!isEmpty(profileData.name))
					? profileData.name
					: ""
			}

			var dataClient = getDataClient();
			dataClient.message("add_profile", {profile: profile})
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static delete(user, profileId) {
		return new Promise((resolve, reject) => {
			Profiles.getById(user, profileId)
				.then((response) => {
					var profile = response.response;

					var dataClient = getDataClient();
					dataClient.message("delete_profile", {id: profile.id})
						.then((dataResponse) => {
							dataClient.close();
							resolve(logicResponse.ok({success: "true"}));
						})
						.catch((error) => {
							dataClient.close();
							reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
						});
				})
				.catch((error) => {
					reject(error);
				})
		});
	}

	static update(user, profileId, update) {
		return new Promise((resolve, reject) => {
			Profiles.getById(user, profileId)
				.then((response) => {
					var profile = response.response;

					// User can update only name, so...
					if (typeof update.name === "string" && profile.name !== update.name) {
						profile.name = update.name;

						var dataClient = getDataClient();
						dataClient.message("update_profile", {id: profile.id, profile: profile})
							.then((dataResponse) => {
								dataClient.close();
								resolve(logicResponse.ok({changes: 1}));
							})
							.catch((error) => {
								dataClient.close();
								reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
							});
					} else {
						resolve(logicResponse.ok({changes: 0}));
					}
				})
				.catch((error) => {
					reject(error);
				})
		});
	}
}

module.exports = Profiles;