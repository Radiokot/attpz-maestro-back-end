var Users = require("./users-functions.js");

function configure(server) {
	server.route("user_register", (data, reply) => {
		var registerData = data.user;

		Users.register(registerData)
			.then((response) => {
				reply(response);
			})
			.catch((error) => {
				reply(error);
			});
	});

	server.route("user_login", (data, reply) => {
		Users.login(data.email, data.password)
			.then((response) => {
				reply(response);
			})
			.catch((error) => {
				reply(error);
			});
	});
}

module.exports.configure = configure;