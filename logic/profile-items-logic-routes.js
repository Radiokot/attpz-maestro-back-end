var errorCodes = require("../util/error-codes.js");
var logicUtil = require("../logic/logic-util.js");
var isEmpty = require("is-empty");
var logicResponse = logicUtil.logicResponse;
var getAuthorizedUser = logicUtil.getAuthorizedUser;

var ProfileItems = require("./profile-items-functions");

function configure(server) {
	server.route("get_profile_items_by_profile_id", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				ProfileItems.getForProfile(data.profileId)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("get_profile_item_intsruments", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;
				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				ProfileItems.getItemInstruments(data.itemId)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("add_profile_item", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				ProfileItems.add(user, data.profileId, data.itemData)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("delete_profile_item", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				ProfileItems.delete(user, data.itemId)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("add_profile_item_instrument", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				ProfileItems.addInstrument(user, data.itemId, data.instrumentData)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});

	server.route("apply_profile_item", (data, reply) => {
		getAuthorizedUser(data.auth)
			.then((userResponse) => {
				var user = userResponse;

				if (isEmpty(user)) {
					reply(logicResponse.error(errorCodes.LOGIC.UNAUTHORIZED, "Unauthorized"));
					return;
				}

				ProfileItems.apply(user, data.itemId)
					.then((response) => {
						reply(response);
					})
					.catch((error) => {
						reply(error);
					});
			})
			.catch((error) => {
				reply(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
			});
	});
}

module.exports.configure = configure;