var socketMessages = require("../util/socket-messages.js");
var errorCodes = require("../util/error-codes.js");
var logicUtil = require("../logic/logic-util.js");
var isEmpty = require("is-empty");
var logicResponse = logicUtil.logicResponse;
var getDataClient = logicUtil.getDataClient;

class InstrumentTypes {
	static getAll() {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("get_instrument_types")
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}

	static getById(id) {
		return new Promise((resolve, reject) => {
			var dataClient = getDataClient();
			dataClient.message("get_instrument_type_by_id", {id: id})
				.then((dataResponse) => {
					dataClient.close();
					resolve(logicResponse.ok(dataResponse));
				})
				.catch((error) => {
					dataClient.close();
					reject(logicResponse.error(errorCodes.LOGIC.DATA_CONNECTION_FAILED, "Data connection failed"));
				});
		});
	}
}

module.exports = InstrumentTypes;