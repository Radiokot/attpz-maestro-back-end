var errorCodes = require("../util/error-codes.js");
var restUtil = require("../rest/rest-util.js");
var apiResponse = restUtil.apiResponse;
var getLogicClient = restUtil.getLogicClient;
var getAuthData = restUtil.getAuthData;

function configure(router) {
	router.route("/instrument-types/")
		.get((req, res) => {
			var client = getLogicClient();
			client.message("get_instrument_types")
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});

	router.route("/instrument-types/:type_id")
		.get((req, res) => {
			var typeId = req.params.type_id;

			var client = getLogicClient();
			client.message("get_instrument_type_by_id", {id: typeId})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});
}

module.exports.configure = configure;