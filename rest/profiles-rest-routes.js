var errorCodes = require("../util/error-codes.js");
var restUtil = require("../rest/rest-util.js");
var apiResponse = restUtil.apiResponse;
var getLogicClient = restUtil.getLogicClient;
var getAuthData = restUtil.getAuthData;

function configure(router) {
	router.route("/profiles")
		.get((req, res) => {
			var authData = getAuthData(req);

			var client = getLogicClient();
			client.message("get_profiles_for_user", {auth: authData})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		})
		.post((req, res) => {
			var authData = getAuthData(req);
			var profileData = req.body;

			var client = getLogicClient();
			client.message("add_profile", {auth: authData, profileData: profileData})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});

	router.route("/profiles/:profile_id")
		.get((req, res) => {
			var authData = getAuthData(req);
			var profileId = req.params.profile_id;

			var client = getLogicClient();
			client.message("get_profile_by_id", {auth: authData, profileId: profileId})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		})
		.patch((req, res) => {
			var authData = getAuthData(req);
			var profileId = req.params.profile_id;
			var update = req.body;

			var client = getLogicClient();
			client.message("update_profile", {auth: authData, profileId: profileId, update: update})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		})
		.delete((req, res) => {
			var authData = getAuthData(req);
			var profileId = req.params.profile_id;

			var client = getLogicClient();
			client.message("delete_profile", {auth: authData, profileId: profileId})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});

	router.route("/profiles/:profile_id/items")
		.get((req, res) => {
			var authData = getAuthData(req);
			var profileId = req.params.profile_id;

			var client = getLogicClient();
			client.message("get_profile_items_by_profile_id", {auth: authData, profileId: profileId})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		})
		.post((req, res) => {
			var authData = getAuthData(req);
			var profileId = req.params.profile_id;
			var itemData = req.body;

			var client = getLogicClient();
			client.message("add_profile_item", {auth: authData, profileId: profileId, itemData: itemData})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});

	router.route("/profiles/:profile_id/items/:item_id")
		.delete((req, res) => {
			var authData = getAuthData(req);
			var itemId = req.params.item_id;

			var client = getLogicClient();
			client.message("delete_profile_item", {auth: authData, itemId: itemId})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});

	router.route("/profiles/:profile_id/items/:item_id/instruments")
		.get((req, res) => {
			var authData = getAuthData(req);
			var itemId = req.params.item_id;

			var client = getLogicClient();
			client.message("get_profile_item_intsruments", {auth: authData, itemId: itemId})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		})
		.post((req, res) => {
			var authData = getAuthData(req);
			var itemId = req.params.item_id;
			var instrumentData = req.body;

			var client = getLogicClient();
			client.message("add_profile_item_instrument", {auth: authData, itemId: itemId,
				instrumentData: instrumentData})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		})

	router.route("/profiles/:profile_id/items/:item_id/apply")
		.get((req, res) => {
			var authData = getAuthData(req);
			var itemId = req.params.item_id;

			var client = getLogicClient();
			client.message("apply_profile_item", {auth: authData, itemId: itemId})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		})
}

module.exports.configure = configure;