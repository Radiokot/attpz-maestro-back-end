var socketMessages = require("../util/socket-messages.js");
var errorCodes = require("../util/error-codes.js");

class ApiResponse {
	static ok(res, data) {
		res.json({response: data});
	}

	static error(res, error) {
		var response = {error: error};
		var status = 500;
		switch (error.code) {
			case errorCodes.LOGIC.UNAUTHORIZED:
				status = 401;
				break;
			case errorCodes.LOGIC.FORBIDDEN:
				status = 403;
				break;
			case errorCodes.LOGIC.INVALID_REQUEST_DATA:
			case errorCodes.REST.PROCESS_REQUEST_FAILED:
				status = 400;
		}
		res.status(status);
		res.json(response);
	}
}

function getLogicClient() {
	return socketMessages.client("127.0.0.1", 8081);
}

function getAuthData(req) {
	return {
		email: req.headers["x-auth-email"],
		key: req.headers["x-auth-key"]
	};
}

module.exports.apiResponse = ApiResponse;
module.exports.getLogicClient = getLogicClient;
module.exports.getAuthData = getAuthData;