var errorCodes = require("../util/error-codes.js");
var socketMessages = require("../util/socket-messages.js");
var restUtil = require("../rest/rest-util.js");
var jsonPatch = require("fast-json-patch");
var apiResponse = restUtil.apiResponse;

var settingsMap = new Map();
var subscribers = [];

function getDataClient() {
	return socketMessages.client("127.0.0.1", 8082);
}

function updateSettings() {
	var instrumentIds = [];
	for (var id of settingsMap.keys()) {
		instrumentIds[instrumentIds.length] = id;
	}

	if (instrumentIds.length > 0) {
		var dataClient = getDataClient();
		dataClient.message("get_instrument_settings_by_ids", {ids: instrumentIds})
			.then((response) => {
				dataClient.close();

				for (var instrument of response) {
					var mapItem = settingsMap.get(instrument.id);

					// If client disconnected 10 sec delete it.
					if (mapItem.request.ended) {
						mapItem.missed++;
						settingsMap.set(instrument.id, mapItem);
						if (mapItem.missed > 30) {
							settingsMap.delete(instrument.id);
							setInstrumentOnline(instrument.id, false);
						}
						continue;
					}

					var diff = jsonPatch.compare(mapItem.settings, instrument.settings);
					if (diff.length > 0) {
						mapItem.settings = instrument.settings;
						mapItem.response.json(instrument.settings);
						settingsMap.set(instrument.id, mapItem);
					}
				}
			})
			.catch((error) => {
				dataClient.close();
			});
	}
}

function setInstrumentOnline(id, isOnline) {
	var dataClient = getDataClient();
	dataClient.message("set_instrument_online", {instrumentId: id, isOnline: isOnline})
		.then((response) => {
			dataClient.close();
		})
		.catch((error) => {
			dataClient.close();
		});
}

function configure(router) {
	setInterval(updateSettings, 300);

	router.route("/instrument-longpoll/:instrument_id")
		.get((req, res) => {
			var instrumentId = parseInt(req.params.instrument_id);

			var prevItem = settingsMap.get(instrumentId);
			settingsMap.set(instrumentId, {
				request: req.connection._writableState,
				response: res,
				settings: (prevItem != null) ? prevItem.settings : {},
				missed: 0
			});
			if (prevItem == null) {
				setInstrumentOnline(instrumentId, true);
			}
		});
}

module.exports.configure = configure;