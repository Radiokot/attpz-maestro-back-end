var errorCodes = require("../util/error-codes.js");
var restUtil = require("../rest/rest-util.js");
var apiResponse = restUtil.apiResponse;
var getLogicClient = restUtil.getLogicClient;

function configure(router) {
	router.route("/users/register")
		.post((req, res) => {
			var user = req.body;

			var client = getLogicClient();
			client.message("user_register", {user: user})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});

	router.route("/users/login")
		.post((req, res) => {
			var email = req.body.email;
			var password = req.body.password;

			var client = getLogicClient();
			client.message("user_login", {email: email, password: password})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});
}

module.exports.configure = configure;