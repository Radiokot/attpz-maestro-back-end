var errorCodes = require("../util/error-codes.js");
var restUtil = require("../rest/rest-util.js");
var apiResponse = restUtil.apiResponse;
var getLogicClient = restUtil.getLogicClient;
var getAuthData = restUtil.getAuthData;

function configure(router) {
	router.route("/instruments")
		.get((req, res) => {
			var authData = getAuthData(req);

			var client = getLogicClient();
			client.message("get_instruments_for_user", {auth: authData})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});

	router.route("/instruments/:instrument_id")
		.get((req, res) => {
			var authData = getAuthData(req);
			var instrumentId = req.params.instrument_id;

			var client = getLogicClient();
			client.message("get_instrument_by_id", {auth: authData, id: instrumentId})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		})
		.patch((req, res) => {
			var authData = getAuthData(req);
			var instrumentId = req.params.instrument_id;
			var update = req.body;

			var client = getLogicClient();
			client.message("update_instrument", {auth: authData, id: instrumentId, update: update})
				.then((response) => {
					if (response.hasOwnProperty("error")) {
						apiResponse.error(res, response.error);
					} else {
						apiResponse.ok(res, response.response);
					}
					client.close();
				})
				.catch((error) => {
					apiResponse.error(res,
						{code: errorCodes.REST.LOGIC_CONNECTION_FAILED, message: "Logic connection failed"});
					client.close();
				});
		});
}

module.exports.configure = configure;