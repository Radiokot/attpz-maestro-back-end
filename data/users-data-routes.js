function addRoutes(server, db) {
	server.route("get_user_by_email", (data, reply) => {
		db.getUserByEmail(data.email).then((rows) => {
			reply(rows[0]);
		});
	});

	server.route("add_user", (data, reply) => {
		db.addUser(data.user).then((rows) => {
			reply(rows);
		});
	});
}

module.exports.configure = addRoutes;