var mysql = require("mysql");
var properties = require("properties");

class Database {
	constructor() {
		var initPool = function (host, port, user, pass, database) {
			this._pool = mysql.createPool({
				connectionLimit: 100,
				host: host,
				port: port,
				user: user,
				password: pass,
				database: database,
				debug: false
			});
		}.bind(this);

		properties.parse("properties", {path: true, namespaces: true}, function (error, props) {
			if (error) return console.error(error);

			var dbProps = props.db;
			initPool(dbProps.host, dbProps.port, dbProps.user, dbProps.pass, dbProps.database);
		});
	}

	// HALLS

	getHalls() {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query("SELECT * FROM halls", (err, rows, fields) => {
					connection.release();
					if (err) reject(err);
					resolve(rows)
				});
			});
		});
	}

	getHallById(id) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query("SELECT * FROM halls WHERE id=?", id, (err, rows, fields) => {
					connection.release();
					if (err) reject(err);
					resolve(rows[0]);
				});
			});
		});
	}

	updateHall(id, hall) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query("UPDATE halls SET name=?, location=? WHERE id=?",
					[hall.name, hall.location, id], (err, rows, fields) => {
						connection.release();
						if (err) reject(err);
						resolve(rows[0]);
					});
			});
		});
	}

	deleteHall(id) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query("DELETE FROM halls WHERE id=?", id, (err, rows, fields) => {
					connection.release();
					if (err) reject(err);
					resolve(rows[0]);
				});
			});
		});
	}

	addHall(hall) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query("INSERT INTO halls VALUES (DEFAULT, ?, ?);",
					[hall.name, hall.location], (err, result, fields) => {
						connection.release();
						if (err) reject(err);
						resolve(result.insertId);
					});
			});
		});
	}

	// USERS

	getUserByEmail(email) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query("SELECT * FROM users WHERE email=?", email, (err, rows, fields) => {
					connection.release();
					if (err) {
						reject(err);
						return;
					}
					resolve(rows);
				});
			});
		});
	}

	addUser(user) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query("INSERT INTO users VALUES (DEFAULT, ?, ?, ?, ?);",
					[user.name, user.email, user.password, user.salt], (err, result, fields) => {
						if (err) {
							connection.release();
							reject(err);
							return;

						}

						connection.query("SELECT id, email FROM users WHERE id=?;",
							result.insertId, (err, rows, fields) => {
								connection.release();
								if (err) {
									reject(err);
									return;
								}
								resolve(rows);
							});
					});
			});
		});
	}

	// INSTRUMENTS

	getInstrumentsByUserId(userId) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`SELECT instruments.id, instruments.name, instruments.online,
				    instrument_types.id as "typeId", instrument_types.name AS "typeName"
				    FROM instruments 
					INNER JOIN instrument_types ON instruments.type_id = instrument_types.id 
					WHERE instruments.user_id=? ORDER BY instruments.id DESC`,
					userId, (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	getInstrumentById(id) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`SELECT instruments.id, instruments.user_id AS "userId",
                    instruments.name, instruments.online, instrument_types.id as "typeId", 
                    instrument_types.name AS "typeName", instruments.settings 
				    FROM instruments 
					INNER JOIN instrument_types ON instruments.type_id = instrument_types.id 
					WHERE instruments.id=?`,
					id, (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	getInstrumentSettingsByIds(ids) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`SELECT id, settings 
				    FROM instruments
					WHERE id IN (?)`,
					[ids], (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	updateInstrument(instrument) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`UPDATE instruments SET name=?, settings=?
					WHERE instruments.id=?`,
					[instrument.name, JSON.stringify(instrument.settings), instrument.id], (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	setInstrumentOnline(id, isOnline) {
		isOnline = (isOnline) ? 1 : 0;
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`UPDATE instruments SET online=?
					WHERE instruments.id=?`,
					[isOnline, id], (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	// INSTRUMENT TYPES

	getInstrumentTypes() {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`SELECT id, name FROM instrument_types`,
					(err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	getInstrumentTypeById(id) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`SELECT id, name, jsonSchema as "schema" FROM instrument_types
                    WHERE id=?`,
					id, (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	// PROFILES

	getProfilesByUserId(userId) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`SELECT id, name, 
       				(SELECT COUNT(*) from profile_items WHERE profile_id = profiles.id) as itemsCount
					FROM profiles 
					WHERE profiles.user_id=? ORDER BY profiles.id DESC`,
					userId, (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	getProfileById(id) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`SELECT id, user_id as userId, name
					FROM profiles 
					WHERE profiles.id=? ORDER BY profiles.id DESC`,
					id, (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	addProfile(profile) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query("INSERT INTO profiles (user_id, name) VALUES (?, ?);",
					[profile.userId, profile.name], (err, result, fields) => {
						if (err) {
							connection.release();
							reject(err);
							return;

						}

						connection.query("SELECT id, name FROM profiles WHERE id=?;",
							result.insertId, (err, rows, fields) => {
								connection.release();
								if (err) {
									reject(err);
									return;
								}
								resolve(rows);
							});
					});
			});
		});
	}

	deleteProfile(profileId) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`DELETE FROM profiles WHERE id=?`,
					profileId, (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	updateProfile(id, data) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`UPDATE profiles SET name=? WHERE id=?`,
					[data.name, id], (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	// PROFILE ITEMS

	getProfileItemsByProfileId(id) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`SELECT id, name FROM profile_items WHERE profile_id = ? ORDER BY item_order`,
					id, (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	getProfileItemInstruments(id) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`SELECT profile_item_instruments.inst_id AS "id", instruments.name, 
						profile_item_instruments.settings FROM profile_item_instruments, instruments 
						WHERE profile_item_instruments.inst_id = instruments.id 
						AND profile_item_instruments.item_id=?`,
					id, (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	addProfileItem(item) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`INSERT INTO profile_items (profile_id, item_order, name) VALUES (?, ?, ?)`,
					[item.profileId, 1, item.name], (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	addProfileItemInstrument(itemId, instrument) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`INSERT INTO profile_item_instruments (item_id, inst_id, settings) VALUES (?, ?, ?)`,
					[itemId, instrument.id, JSON.stringify(instrument.settings)], (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}

	deleteProfileItem(itemId) {
		return new Promise((resolve, reject) => {
			this._pool.getConnection((err, connection) => {
				connection.query(`DELETE FROM profile_items WHERE id=?`,
					itemId, (err, rows, fields) => {
						connection.release();
						if (err) {
							reject(err);
							return;
						}
						resolve(rows);
					});
			});
		});
	}
}

module.exports = new Database();