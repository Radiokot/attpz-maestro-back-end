function addRoutes(server, db) {
	server.route("get_profiles_by_user_id", (data, reply) => {
		db.getProfilesByUserId(data.userId).then((rows) => {
			reply(rows);
		});
	});

	server.route("get_profile_by_id", (data, reply) => {
		db.getProfileById(data.id).then((rows) => {
			reply(rows[0]);
		});
	});

	server.route("add_profile", (data, reply) => {
		db.addProfile(data.profile).then((rows) => {
			reply(rows[0]);
		});
	});

	server.route("delete_profile", (data, reply) => {
		db.deleteProfile(data.id).then((rows) => {
			reply(rows);
		});
	});

	server.route("update_profile", (data, reply) => {
		db.updateProfile(data.id, data.profile).then((rows) => {
			reply(rows[0]);
		});
	});
}

module.exports.configure = addRoutes;