var cache = require("memory-cache");

const CAHCE_SETTINGS_KEY = "_SETTINGS";

function addRoutes(server, db) {
	server.route("get_instruments_by_user_id", (data, reply) => {
		db.getInstrumentsByUserId(data.userId).then((rows) => {

			// Add type property and parse settings.
			for (var i = 0; i < rows.length; i++) {
				var instrument = rows[i];
				rows[i].type = {
					id: instrument.typeId,
					name: instrument.typeName
				}
				rows[i].online = instrument.online == 1;
				rows[i].typeId = undefined;
				rows[i].typeName = undefined;
			}
			reply(rows);
		});
	});

	server.route("get_instrument_by_id", (data, reply) => {
		db.getInstrumentById(data.id).then((rows) => {

			// Add type property and parse settings.
			for (var i = 0; i < rows.length; i++) {
				var instrument = rows[i];
				rows[i].type = {
					id: instrument.typeId,
					name: instrument.typeName
				}
				rows[i].online = instrument.online == 1;
				rows[i].typeId = undefined;
				rows[i].typeName = undefined;
				rows[i].settings = JSON.parse(rows[i].settings);
			}

			reply(rows[0]);
		});
	});

	server.route("update_instrument", (data, reply) => {
		cache.put(data.instrument.id + CAHCE_SETTINGS_KEY, data.instrument.settings);
		db.updateInstrument(data.instrument).then((rows) => {
			reply(rows);
		});
	});

	server.route("set_instrument_online", (data, reply) => {
		db.setInstrumentOnline(data.instrumentId, data.isOnline).then((rows) => {
			reply(rows);
		});
	});

	server.route("get_instrument_settings_by_ids", (data, reply) => {
		let ids = data.ids;
		let queryIds = [];
		let result = [];

		for (let id of ids) {
			let key = id + CAHCE_SETTINGS_KEY;
			let cached = cache.get(key);

			if (cached == null) {
				queryIds[queryIds.length] = id;
			} else {
				result[result.length] = {
					id: id,
					settings: cached
				};
			}
		}

		if (queryIds.length > 0) {
			db.getInstrumentSettingsByIds(queryIds).then((rows) => {
				for (let row of rows) {
					let item = {
						id: row.id,
						settings: JSON.parse(row.settings)
					};
					cache.put(row.id + CAHCE_SETTINGS_KEY, item.settings);
					result[result.length] = item;
				}
				reply(result);
			});
		} else {
			reply(result);
		}
	});
}

module.exports.configure = addRoutes;