function addRoutes(server, db) {
	server.route("get_instrument_types", (data, reply) => {
		db.getInstrumentTypes().then((rows) => {
			reply(rows);
		});
	});

	server.route("get_instrument_type_by_id", (data, reply) => {
		db.getInstrumentTypeById(data.id).then((rows) => {
			if (rows.length > 0) {
				rows[0].schema = JSON.parse(rows[0].schema);
			}
			reply(rows[0]);
		});
	});
}

module.exports.configure = addRoutes;