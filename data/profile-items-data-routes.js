function addRoutes(server, db) {
	server.route("get_profile_items_by_profile_id", (data, reply) => {
		db.getProfileItemsByProfileId(data.profileId).then((rows) => {
			reply(rows);
		});
	});

	server.route("get_profile_item_intsruments", (data, reply) => {
		db.getProfileItemInstruments(data.itemId).then((rows) => {
			for (let i = 0; i < rows.length; i++) {
				rows[i].settings = JSON.parse(rows[i].settings);
			}
			reply(rows);
		});
	});

	server.route("add_profile_item", (data, reply) => {
		db.addProfileItem(data.item).then((rows) => {
			reply(rows);
		});
	});

	server.route("delete_profile_item", (data, reply) => {
		db.deleteProfileItem(data.itemId).then((rows) => {
			reply(rows);
		});
	});

	server.route("add_profile_item_instrument", (data, reply) => {
		db.addProfileItemInstrument(data.itemId, data.instrument).then((rows) => {
			reply(rows);
		});
	});
}

module.exports.configure = addRoutes;