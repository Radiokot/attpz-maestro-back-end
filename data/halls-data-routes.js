function addRoutes(server, db) {
	server.route('get_halls', (data, reply) => {
		db.getHalls().then((rows) => {
			reply(rows);
		});
	});

	server.route('get_hall_by_id', (data, reply) => {
		db.getHallById(data.id).then((row) => {
			reply(row);
		});
	});

	server.route('add_hall', (data, reply) => {
		db.addHall(data.hall).then((row) => {
			reply(row);
		});
	});

	server.route('delete_hall', (data, reply) => {
		db.deleteHall(data.id).then((row) => {
			reply(row);
		});
	});
}

module.exports.configure = addRoutes;