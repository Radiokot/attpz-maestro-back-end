// Set up server for responding.
var net = require("net");
var JsonSocket = require("json-socket");
var SocketRouter = require("socket-router");

var server = new SocketRouter.Server();
var socketServer = net.createServer();
var port = process.env.PORT || 8081;
socketServer.listen(port);
JsonSocket.prototype.send = JsonSocket.prototype.sendMessage;
socketServer.on("connection", (socket) => {
	server.listen(new JsonSocket(socket));
});

var userRoutes = require("./logic/users-logic-routes.js");
userRoutes.configure(server);

var instrumentsRoutes = require("./logic/instruments-logic-routes.js");
instrumentsRoutes.configure(server);

var instrumentTypesRoutes = require("./logic/instrument-types-logic-routes");
instrumentTypesRoutes.configure(server);

var profilesRoutes = require("./logic/profiles-logic-routes");
profilesRoutes.configure(server);

var profileItemsRoutes = require("./logic/profile-items-logic-routes");
profileItemsRoutes.configure(server);

console.log("Logic layer instance is running on port " + port);