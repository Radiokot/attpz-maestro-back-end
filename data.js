var db = require("./data/db.js");

var net = require("net");
var JsonSocket = require("json-socket");
var SocketRouter = require("socket-router");

var server = new SocketRouter.Server();
var socketServer = net.createServer();
var port = process.env.PORT || 8082;
socketServer.listen(port);
JsonSocket.prototype.send = JsonSocket.prototype.sendMessage;
socketServer.on("connection", (socket) => {
	server.listen(new JsonSocket(socket));
});

// Add routes for "halls"
var hallsRoutes = require("./data/halls-data-routes.js");
hallsRoutes.configure(server, db);

// Add routes for "users"
var usersRoutes = require("./data/users-data-routes.js");
usersRoutes.configure(server, db);

// Add routes for instruments
var instrumentsRoutes = require("./data/instruments-data-routes.js");
instrumentsRoutes.configure(server, db);

// Add routes for instrument types
var instrumentTypesRoutes = require("./data/instrument-types-data-routes");
instrumentTypesRoutes.configure(server, db);

// Add routes for profiles
var profilesRoutes = require("./data/profiles-data-routes");
profilesRoutes.configure(server, db);

// Add routes for profiles
var profileItemsRoutes = require("./data/profile-items-data-routes");
profileItemsRoutes.configure(server, db);

console.log("Data access layer instance is running on port " + port);